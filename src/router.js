import Vue from 'vue'
import Router from 'vue-router'
import Guide from './views/Guide'
import Style from './views/Style'
import API from './views/API'
import Cases from './views/Cases'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Guide
    },
    {
      path: '/guide',
      name: 'guide',
      component: Guide
    },
    {
      path: '/guide/:guideId',
      name: 'guide2',
      component: Guide
    },
    {
      path: '/style',
      name: 'style',
      component: Style
    },
    {
      path: '/style/:styleId',
      name: 'style2',
      component: Style
    },
    {
      path: '/api',
      name: 'api',
      component: API
    },
    {
      path: '/api/:apiId',
      name: 'api2',
      component: API
    },
    {
      path: '/cases',
      name: 'case',
      component: Cases
    },
    {
      path: '/cases/:caseId',
      name: 'example',
      component: Cases
    }
  ]
})
