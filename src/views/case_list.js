export default [
  {
    id: 'full_case',
    name: '一个完整的大屏',
    des: '一个完整的静态大屏',
    thumb: ''
  },
  {
    id: 'autoplay',
    name: '组件间的联动',
    des: '通过配置实现两个组件间的联动响应',
    thumb: ''
  },
  {
    id: 'filter',
    name: '数据过滤',
    des: '通过配置简单的过滤条件对数据进行进一步过滤',
    thumb: ''
  },
  {
    id: 'aggregate',
    name: '数据聚合',
    des: '对数据按某一字段进行求和、求平均等聚合操作',
    thumb: ''
  },
  {
    id: 'sort',
    name: '数据排序',
    des: '通过配置简单的排序条件对数据一个或多个字段进行排序',
    thumb: ''
  },
  {
    id: 'auto_add',
    name: '使用API添加组件',
    des: '通过调用API接口3秒后在大屏上添加一个柱状图',
    thumb: ''
  },
  {
    id: 'auto_switch',
    name: '标签页的自动切换',
    des: '通过脚本实现标签页的自动切换',
    thumb: ''
  },
  {
    id: 'custom',
    name: '自定义组件',
    des: '根据需要定制自己的可视化组件',
    thumb: ''
  },
  {
    id: 'map',
    name: '引入配图系统中的地图',
    des: '通过地图组件可以直接将配图系统中的专题地图引入到大屏',
    thumb: ''
  },
  {
    id: 'map_bar',
    name: '地图上叠加柱状专题图',
    des: '在地图组件上叠加柱状专题图图层',
    thumb: ''
  },
  {
    id: 'map_pie',
    name: '地图上叠加饼状专题图',
    des: '在地图组件上叠加饼状专题图图层',
    thumb: ''
  },
  {
    id: 'drilling',
    name: '行政区划图上钻下取',
    des: '在分级设色图组件上增加上钻下取功能',
    thumb: ''
  }
]