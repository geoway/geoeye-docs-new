# 事件

### screen事件

#### `on-mouseenter`

鼠标进入到组件内部时触发，事件传递的数据如下：

```
{
  id: 'SJOgvSLoW',                 // 当前组件的Id
  data: componentConfig            // 组件的完整配置
}
```

#### `on-mouseleave`

鼠标离开组件时触发，事件传递的数据如下：

```
{
  id: 'SJOgvSLoW',                 // 当前组件的Id
  data: componentConfig            // 组件的完整配置
}
```

#### `on-click`

鼠标点击组件时触发，事件传递的数据如下：

```
{
  id: 'SJOgvSLoW',                 // 当前组件的Id
  data: componentConfig            // 组件的完整配置
}
```


### components事件

#### `source.load`
组件上绑定的source加载并转换完成时触发，事件传递的数据如下：
```
{
  id: 'SJOgvSLoW',      // 组件的Id
  data: data            // 已转换完成的数据
}
```

```js
var component = screen.getComponent('SJOgvSLoW');
component.on('source.load', e => {
  // do something with data
  console.log(e.data);
})
```
#### `render`
组件完成渲染时被触发，事件传递的参数为当前组件的实例：
```
{
  id: 'SJOgvSLoW',  // 组件Id
  name: '柱状图',   // 组件的名称
  type: 'bar',      // 组件的类型
  layer: {……},      // 组件的配置参数
  el: HTMLElement,  // 组件所在的DOM元素
  ……
}
```

```js
var component = screen.getComponent('SJOgvSLoW');
component.on('render', e => {
  console.log(e.name + '组件已完成渲染');
});
// => 柱状图组件已完成渲染
```