# 基本用法

可视化引擎的API提供一些常用的方法和事件，便于开发者对大屏进行更高层次的定制，例如实现更复杂的交互或联动。

### 大屏初始化

```js
var config = {
  scale: 'height',
  width: 1920,
  height: 1080,
  components: []
}
var screen = new Geoeye.Screen(config, options);
```
**Options选项**

选项 | 类型 | 功能
--- | --- | ---
container | String 或 HTMLElement | 绘制大屏的容器

通过`new Geoeye.Screen()` 初始化以后会返回一个 `Screen` 实例，大屏的所有接口和事件都通过该实例对象调用。

**删除一个组件**

```js
screen.removeComponent(componentId);
```

**监听事件**

```js
screen.on('on-click', e => {
  let layerId = e.data.id
  console.log(`点击了图表${layerId}`)
})
```
