# 方法

### `.setConfig(Object: config)`

参数 | 类型 | 是否必须 | 说明
--- | --- | --- | ---
config | Object | 是 | 大屏的配置文件

给定一个大屏的样式文件，重新绘制整个大屏，具体配置项可查看大屏的[样式规范](#/style/style-overview)

示例:

```js
var newStyle = {
  scale: 'width',
  width: 1366,
  height: 768,
  components: []
}
screen.setConfig(newStyle);
```

---

### `.addComponent(Object: config, String: beforeId)`
参数 | 类型 | 是否必须 | 说明
--- | --- | --- | ---
config | Object | 是 | 组件的配置参数
beforeId  | String | 否 | 将新组件插入到已存在的某个组件前面，如果不指定则新组件将插入到最后面

向大屏中添加一个组件

示例:

```js
var component = {
  "id": "SJOgvSLoW",
  "name": "柱状图",
  "type": "bar",
  "source": {
    "url": "http://localhost:8080/debug/testdata.json",
    "type": "table",
    "transform": [{
      "type": "filter",
      "condition": [">", "gdp", ["get", "pop"]]
    }]
  },
  "paint": {
    "title": {
      "text": "基本柱状图",
      "fontColor": "#ffffff",
      "fontSize": 18
    },
    "xAxis": {
      "field": "city"
    },
    "series": [
      {
        "name": "人口",
        "field": "pop"
      }
    ]
  },
  "layout": {
    "width": 300,
    "height": 200,
    "offsetX": 50,
    "offsetY": 50,
    "visible": true
  }
}
screen.addComponent(component);
```

---

### `.removeComponent(String: componentId)`
参数 | 类型 | 是否必须 | 说明
--- | --- | --- | ---
componentId | String | 是 | 要移除的组件ID

移除指定Id的组件

```js
screen.removeComponent('SJOgvSLoW');
```

---

###  `.moveComponent(String: componentId, String: beforeId)`
参数 | 类型 | 是否必须 | 说明
--- | --- | --- | ---
componentId | String | 是 | 要移动的组件Id
beforeId | String | 是 | 目标位置的组件Id

将id为"componentId"的组件移动到id为"beforeId"的组件前面。

---

### `.getComponent(String: componentId) ->  Object`
参数 | 类型 | 是否必须 | 说明
--- | --- | --- | ---
componentId | String | 是 | 要获取的组件Id

根据组件的id获取该组件的实例对象

```js
var component = screen.getComponent('SJOgvSLoW');
console.log(component);
//=>{id: 'SJOgvSLoW', name: '柱状图', type: 'bar', layer: {……}, el: HTMLElement}
```

---

### `.getComponents() ->  Array`

获取大屏中所有组件的实例对象列表

```js
var chatrts = screen.getComponents();
console.log(components);
//=> [{……}, {……}, ···]
```

---

### `.getConfig() ->  Object`

获取大屏的样式

```js
var style = screen.getConfig();
console.log(style);
//=> {scale: 'height', width: 1920, height: 1080, components: [] }
```

---

### `.setWidth(Number: width)`
参数 | 类型 | 是否必须 | 说明
--- | --- | --- | ---
width | Number | 是 | 大屏的宽度

重设大屏的宽度

```js
screen.setWidth(1000);
```

---

### `.setHeight(Number: height)`
参数 | 类型 | 是否必须 | 说明
--- | --- | --- | ---
height | Number | 是 | 大屏的高度

重设大屏的高度

```js
screen.setHeight(600);
```

---

### `.setZoomMode(String: scale)`
参数 | 类型 | 是否必须 | 说明
--- | --- | --- | ---
scale | String | 是 | 大屏的缩放模式，可取值"width"、"height"、"full"

重设大屏的缩放模式：按宽度缩放(width)、按高度缩放(height)、全屏铺满(full)

```js
screen.setZoomMode('width');
```

---

### `.setBackground(String: color)`
参数 | 类型 | 是否必须 | 说明
--- | --- | --- | ---
color | String | 是 | 大屏的背景色

设置大屏的背景色，color颜色值参考css支持的颜色格式

```js
screen.setBackground('#333333');
```

---

### `.setLayoutProperty(String: componentId, String: name, Any: value)`
参数 | 类型 | 是否必须 | 说明
--- | --- | --- | ---
componentId | String | 是 | 要设置的组件Id
name | String | 是 | 要设置的布局属性的名称
value | Any | 是 | 要设置的布局属性的值

设置组件的布局属性，具体的属性名称可以参考[布局配置](#/style/style-layout)

```js
screen.setLayoutProperty('SJOgvSLoW', "width", 300);    //组件宽度设为300px
screen.setLayoutProperty('SJOgvSLoW', "visible", false);//组件设为不可见
```

---

### `.setPaintProperty(String: componentId, String: name, Any: value)`
参数 | 类型 | 是否必须 | 说明
--- | --- | --- | ---
componentId | String | 是 | 要设置的组件Id
name | String | 是 | 要设置的渲染属性的名称
value | Any | 是 | 要设置的渲染属性的值

设置组件的渲染属性，具体的属性详情可以参考[样式规范](#/style/style-overview)中的各组件配置。

```js
screen.setPaintProperty('SJOgvSLoW', "title", {text: "2017年度全国各省GDP"});
```

---

### `.setLayout(String: componentId, Object: layout)`
参数 | 类型 | 是否必须 | 说明
--- | --- | --- | ---
componentId | String | 是 | 要设置的组件Id
layout | Object | 是 | 要设置的布局属性

设置组件的布局属性，具体的属性名称可以参考[布局配置](#/style/style-layout)

```js
var newLayout = {
  width: 400,
  height: 300,
  offsetX: 0,
  offsetY: 0,
  visible: true
}
screen.setLayout('SJOgvSLoW', newLayout);
```

---

### `.setPaint(String: componentId, Object: paint)`
参数 | 类型 | 是否必须 | 说明
--- | --- | --- | ---
componentId | String | 是 | 要设置的组件Id
paint | Object | 是 | 要设置的渲染属性

设置组件的渲染属性，具体的属性详情可以参考[样式规范](#/style/style-overview)中的各组件配置

```js
var newPaint = {
  fontColor: "#ffffff",
  bold: true
}
screen.setPaint('SJOgvSLoW', newPaint);
```

---

### `.setSource(String: componentId, Object: source)`
参数 | 类型 | 是否必须 | 说明
--- | --- | --- | ---
componentId | String | 是 | 要设置的组件Id
source | Object | 是 | 要设置的数据源对象

数据源具体属性可以参考[数据源配置](#/style/style-source)

```js
var newSource = {
  fontColor: "#ffffff",
  bold: true
}
screen.setPaint('SJOgvSLoW', newPaint);
```

---

### `.setEvent(String: componentId, Object: event)`
参数 | 类型 | 是否必须 | 说明
--- | --- | --- | ---
componentId | String | 是 | 要设置的组件Id
event | Object | 是 | 要设置的联动事件

```js
var newEvent = {
  "year": {
    "alias": "year",
    "default": "2017"
  }
}
screen.setEvent('SJOgvSLoW', newEvent);
```

---

### `.getLayoutProperty(String: componentId, String: name) -> Any`
参数 | 类型 | 是否必须 | 说明
--- | --- | --- | ---
componentId | String | 是 | 组件Id
name | String | 是 | 要获取的布局属性的名称

获取组件的布局属性值

```js
screen.getLayoutProperty('SJOgvSLoW', "width");
//=> 300
```

---

### `.getPaintProperty(String: componentId, String: name) -> Any`
参数 | 类型 | 是否必须 | 说明
--- | --- | --- | ---
componentId | String | 是 | 组件Id
name | String | 是 | 要获取的渲染属性的名称

获取组件的渲染属性值

```js
screen.getPaintProperty('SJOgvSLoW', "title");
//=> {text: "2017年度全国各省GDP"}
```

---

### `.getWidth() -> Number`

获取当前大屏的宽度

```js
screen.getWidth();
//=> 1920
```

---

### `.getHeight() -> Number`

获取当前大屏的高度

```js
screen.getHeight();
//=> 1080
```

---

### `.getZoomMode() -> String`

获取当前大屏的缩放模式

```js
screen.getZoomMode();
//=> "width"
```

---

### `.getScale() -> Object`

获取当前大屏的缩放比例

```js
screen.getScale();
//=> {scaleX: 0.45, scaleY: 0.8}
```

---

### `.resize()`

根据容器的大小更新大屏的大小和位置

```js
screen.resize();
```

---

### `.destroy()`

销毁大屏，移除当前大屏的所有组件

```js
screen.destroy();
```

---