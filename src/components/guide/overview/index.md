# geoeye-js

  基于纯js的大数据可视化渲染引擎，采用可配置的方式快速将配置文件渲染成可视化大屏。
  - 实时数据接入
  - 前端数据清洗
  - 专业的地图可视化
  - 支持组件间的联动更新
  - 集中式数据状态管理
  - 丰富的接口支持定制开发

## 快速开始
引入外部依赖库。为了避免引擎体积过大，本项目并未包含以下依赖包，开发者可根据自己实际需要选择引用必要的外部依赖包。
```html
<!-- 如果要使用地图组件，需要引入@geoway/vmap -->
<script src="//unpkg.com/@geoway/vmap/dist/vmap.min.js"></script>
<!-- 如果要使用echarts图表组件，例如柱状图、折线图等，需要引入echarts -->
<script src="echarts.min.js"></script>
<!-- 如果要使用水球组件，需要引入echarts和echarts-liquidfill -->
<script src="echarts-liquidfill.min.js"></script>
<!-- 如果要使用词云组件，需要引入echarts和echarts-wordcloud -->
<script src="echarts-wordcloud.min.js"></script>
```

引入geoeye引擎和css样式
```html
<link href="//unpkg.com/geoeye-js/dist/geoeye.css" rel="stylesheet" />
<script src="//unpkg.com/geoeye-js/dist/geoeye.min.js"></script>
```

获取大屏配置文件渲染可视化大屏

```html
<div id="container"></div>
<link href="//unpkg.com/geoeye-js/dist/geoeye.css" rel="stylesheet" />
<script src="//unpkg.com/geoeye-js/dist/geoeye.min.js"></script>
<script>
  window.onload = function() {
    const screen = new Geoeye.Screen({
      config: data, 
      container: 'container'
    });
  }
</script>
```

## 文档示例

查看[样式规范](./#/style)

查看[API参考](./#/api)

查看[示例](./#/example)

## 关于示例

本文档中的示例数据纯属虚构，用于展示引擎的用法，数据本身没有实际意义。