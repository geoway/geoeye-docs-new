# CDN引入

通过[unpkg.com/geoeye-js](//unpkg.com/geoeye-js)可以看到`geoeye-js`最新的版本，也可以切换到其他版本选择需要的资源，在页面上引入js和css即可使用。如果需要使用echarts组件或地图组件，可能需要引入额外的依赖包。

```html
<!-- css -->
<link href="//unpkg.com/geoeye-js/dist/geoeye.css" rel="stylesheet" />
<!-- js -->
<script src="//unpkg.com/geoeye-js/dist/geoeye.min.js"></script>
```

### 示例
通过CDN可快速写出一个大屏示例
```html
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>geoeye example</title>
  <link href="//unpkg.com/geoeye-js/dist/geoeye.css" rel="stylesheet" />
  <script src="//unpkg.com/geoeye-js/dist/geoeye.min.js"></script>
  <style>
    body{
      margin: 0;
      padding: 0;
    }
    #geoeye-container{
      position: absolute;
      width: 100%;
      height: 100%;
    }
  </style>
</head>
<body>
<div id="geoeye-container"></div>
<script>
  const config = {
    width: 1920,
    height: 1080,
    scale: 'width',
    background: {
      color: '#0f4b8a'
    },
    components: []
  }
  const screen = new Geoeye.Screen({
    config, 
    container: 'geoeye-container'
  });
</script>
</body>
</html>
```