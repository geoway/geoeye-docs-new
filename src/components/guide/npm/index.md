# npm安装

```
npm install geoeye-js
```
或者
```
yarn add geoeye-js
```

### 示例

如果在vue中使用，可以快速构建一个geoeye组件

```html
<template>
  <div id="geoeye-container"></div>
</template>

<script>
import {Screen} from 'geoeye-js'
export default {
  mounted() {
    const config = {
      width: 1920,
      height: 1080,
      scale: 'width',
      background: {
        color: '#0f4b8a'
      },
      components: []
    }
    const screen = new Screen({
      config, 
      container: 'geoeye-container'
    });
  }
}
</script>

<style>
  @import 'geoeye-js/dist/geoeye.css'
</style>
```
