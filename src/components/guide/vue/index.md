# 在 Vue 中使用

为了更方便的在 vue 中使用，引擎封装了独立的 vue 组件，可以直接注册到项目中使用。

### 安装 geoeye 引擎

```
npm install geoeye-js
```

或者

```
yarn add geoeye-js
```

### 在 main.js 中引入 css 并注册组件

```js
// main.js
import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./vuex/store";

import { VueGeoeye } from "geoeye-js";
import "geoeye-js/dist/geoeye.css";

Vue.component("Geoeye", VueGeoeye);

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
```

### 在我的组件中使用

```html
// MyComponent.vue
<template>
  <div id="geoeye-container">
    <Geoeye
      :config="myConfig"
      ref="geoeye"
      @on-click="clickComponent"
      @on-mousedown="mouseDown"
      @on-render="rendered"
      @on-error="catchError"
    />
  </div>
</template>

<script>
  export default {
    methods: {
      clickComponent(e) {
        let {event, data} = e
        let myScreen = this.$refs.geoeye.screen   // 场景实例
        let component = myScreen.getComponent(data.id)  // 组件实例
      },
      mouseDown(e) {
        let {event, data} = e
      },
      rendered() {

      },
      catchError(err) {
        console.error(err.message)
      }
    }
    mounted() {

    },
    data() {
      return {
        myConfig: 'http://121.41.205.54:1234/api/scenes/v1/dev/bgJRWwiBsGW' // 可以是url或者JSON对象
      }
    }
  }
</script>

<style scoped>
  #geoeye-container {
    width: 1920px;
    height: 1080px;
  }
</style>
```
