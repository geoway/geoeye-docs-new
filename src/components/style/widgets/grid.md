### grid

上下左右的边距，类型`Object`

属性 | 说明 | 类型 | 默认值
--- | --- | --- | ---
left | 左边距，可以是百分比或具体的像素值 | String \| Number| 10%
right | 右边距，可以是百分比或具体的像素值 | String \| Number| 10%
top | 上边距，可以是百分比或具体的像素值 | String \| Number| 40
bottom | 下边距，可以是百分比或具体的像素值 | String \| Number| 20
