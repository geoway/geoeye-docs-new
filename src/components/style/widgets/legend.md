### legend

图例，类型`Object`

属性 | 说明 | 类型 | 默认值
--- | --- | --- | ---
show | 显示图例 | Boolean | true
fontColor | 图例文字的颜色 | String | #ffffff
fontSize | 图例文字的大小 | Number | 12
orient | 排列方式，可取值`horizontal`（水平排列） `vertical`（垂直排列） | Enum String | horizontal
left | 距离左边的位置，可取值`auto` `left` `right` `center`，也可以是具体的像素值 | Enum String \| Number| auto
top | 距离顶部的位置，可取值`auto` `top` `bottom` `middle`，也可以是具体的像素值 | Enum String \| Number| auto
itemWidth | 图例色块的宽度 | Number| 25
itemHeight | 图例色块的高度 | Number| 14
align | 图例文字的对齐方式，可取值`left` `right` | Enum String| left
itemGap | 图例之间的间隔 | Number| 10
formatLabel | 图例的文字是否进行格式化（文字超过3个字符将会省略超过的部分，例如“居民生产总值”会显示为“居民生…”） | Boolean| true
