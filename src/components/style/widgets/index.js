import title from './title.md'
import legend from './legend.md'
import grid from './grid.md'
import tooltip from './tooltip.md'
import xAxis from './xAxis.md'
import yAxis from './yAxis.md'

export {
  title,
  legend,
  grid,
  tooltip,
  xAxis,
  yAxis
}