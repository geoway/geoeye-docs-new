### title

标题，类型`Object`

属性 | 说明 | 类型 | 默认值
--- | --- | --- | ---
text | 标题内容 | String | ''
fontColor | 标题的颜色 | String | #ffffff
fontColor | 标题的大小 | Number | 18
