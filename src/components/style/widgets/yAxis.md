### yAxis

Y轴（数值轴），类型`Object`

属性 | 说明 | 类型 | 默认值
--- | --- | --- | ---
show | 显示坐标轴 | Boolean | true
name | 轴标题 | String | ''
nameColor | 轴标题的颜色 | String | #ffffff
nameSize | 轴标题的大小 | Number | 14
showLine | 显示轴线 | Boolean | true
lineColor | 轴线的颜色 | String | #ffffff
showLabel | 显示标注 | Boolean | true
labelColor | 轴线的颜色 | String | #ffffff
labelSize | 标注的大小 | Number | 14
showGrid | 显示坐标轴指示线 | Boolean | false
gridWidth | 指示线的宽度 | Number | 1
gridType | 指示线的类型，可取值`solid` `dashed` `dotted` | Enum String | solid
gridColor | 指示线的颜色 | String | #333333
position | 坐标轴的位置，可取值`left` `right` | Enum String | left
min | 最小值 | Number | 0
max | 最大值 | Number | 自动
interval | 标注间隔 | Number | 自动
formatLabel | 标注是否进行格式化（文字超过3个字符将会省略超过的部分，例如“居民生产总值”会显示为“居民生…”） | Boolean| true
inverse | 坐标轴反向，坐标轴原点默认在下边，反向后原点在上边 | Boolean | false