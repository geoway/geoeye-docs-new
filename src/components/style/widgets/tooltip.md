### tooltip

提示框，类型`Object`

属性 | 说明 | 类型 | 默认值
--- | --- | --- | ---
show | 鼠标悬浮时显示提示框 | Boolean | true
fomatter | 提示框的内容格式，参考[echarts文档](https://echarts.apache.org/zh/option.html#tooltip.formatter) | String | echarts默认
textColor | 文字颜色 | String | #ffffff
textSize | 文字大小 | Number | 14
