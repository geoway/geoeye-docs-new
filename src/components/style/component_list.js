export default {
  basic: {
    name: '基础组件',
    items: [
      {
        type: 'text',
        name: '文本组件',
        icon: 'md-document'
      },
      {
        type: 'timer',
        name: '计时器',
        icon: 'md-document'
      },
      {
        type: 'image',
        name: '图片',
        icon: 'md-document'
      },
      {
        type: 'video',
        name: '视频',
        icon: 'md-document'
      },
      {
        type: 'web',
        name: '网页',
        icon: 'md-document'
      },
      {
        type: 'marquee',
        name: '跑马灯',
        icon: 'md-document'
      },
      {
        type: 'scroll_list',
        name: '滚动列表',
        icon: 'md-document'
      },
      {
        type: 'word_cloud',
        name: '词云',
        icon: 'md-document'
      },
      {
        type: 'slider',
        name: '轮播',
        icon: 'md-document'
      },
      {
        type: 'shape',
        name: '基本形状',
        icon: 'md-document'
      }
    ]
  },
  charts: {
    name: '图表组件',
    items: [
      {
        type: 'bar',
        name: '柱状图',
        icon: 'md-document'
      },
      {
        type: 'bar_custom',
        name: '象形柱状图',
        icon: 'md-document'
      },
      {
        type: 'bar_h',
        name: '条形图',
        icon: 'md-document'
      },
      {
        type: 'line',
        name: '折线图',
        icon: 'md-document'
      },
      {
        type: 'double_axis',
        name: '双轴图',
        icon: 'md-document'
      },
      {
        type: 'scatter',
        name: '散点图',
        icon: 'md-document'
      },
      {
        type: 'pie',
        name: '饼图/环图',
        icon: 'md-document'
      },
      {
        type: 'radar',
        name: '雷达图',
        icon: 'md-document'
      },
      {
        type: 'funnel',
        name: '漏斗图',
        icon: 'md-document'
      },
      {
        type: 'table',
        name: '表格',
        icon: 'md-document'
      }
    ]
  },
  indicators: {
    name: '指标组件',
    items: [
      {
        type: 'metric',
        name: '文本指标',
        icon: 'md-document'
      },
      {
        type: 'flop',
        name: '翻牌器',
        icon: 'md-document'
      },
      {
        type: 'progress_bar',
        name: '进度条',
        icon: 'md-document'
      },
      {
        type: 'circle',
        name: '环形进度条',
        icon: 'md-document'
      },
      {
        type: 'gauge',
        name: '仪表盘',
        icon: 'md-document'
      },
      {
        type: 'liquid_fill',
        name: '水球图',
        icon: 'md-document'
      }
    ]
  },
  interactions: {
    name: '交互组件',
    items: [
      {
        type: 'select',
        name: '下拉选择框',
        icon: 'md-document'
      },
      {
        type: 'button',
        name: '按钮',
        icon: 'md-document'
      },
      {
        type: 'button_group',
        name: '组合按钮',
        icon: 'md-document'
      },
      {
        type: 'date_picker',
        name: '日期选择',
        icon: 'md-document'
      },
      {
        type: 'timeline',
        name: '时间轴',
        icon: 'md-document'
      },
    ]
  },
  layout: {
    name: '布局组件',
    items: [
      {
        type: 'container',
        name: '容器组件',
        icon: 'md-document'
      },
      {
        type: 'tab',
        name: '切换按钮',
        icon: 'md-document'
      }
    ]
  },
  map: {
    name: '地图组件',
    items: [
      {
        type: 'map',
        name: '地图',
        icon: 'md-document'
      },
      {
        type: 'legend',
        name: '地图图例',
        icon: 'md-document'
      }
    ]
  },
  custom: {
    name: '自定义组件',
    items: [
      {
        type: 'admin_drilling',
        name: '行政区划钻取',
        icon: 'md-document'
      }
    ]
  }
}
