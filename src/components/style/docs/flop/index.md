## 类型
```
  "type": "flop"
```
## paint属性

```
  "paint": {
    common: {...},
    prefix: {...},
    number: {...},
    suffix: {...}
  }
```
### common
通用属性

属性 | 说明 | 类型 | 默认值
--- | --- | --- | ---
orient | 排列方式，可取值`horizontal`或`vertical` | Enum String | horizontal
textAlign | 对齐方式，可以是`left`、`center`或`right` | String | left
interval | 数值与前后缀之间的间隔，单位`px` | Number | 10

### prefix
前缀

属性 | 说明 | 类型 | 默认值
--- | --- | --- | ---
content | 前缀的内容 | String | ￥
fontSize | 文字大小，单位`px` | Number | 48
fontColor | 文字颜色 | String | #ffffff
bold | 是否加粗 | Boolean | false

### number
数值

属性 | 说明 | 类型 | 默认值
--- | --- | --- | ---
fontFamily | 数值的字体，可取值：`LED`、`Futura`、`DIN`、`Montserrat` | Enum String | LED
fontSize | 文字大小，单位`px` | Number | 48
fontColor | 文字颜色 | String | #00ccff
bold | 是否加粗 | Boolean | false
decimalDigits | 小数点后保留的位数 | Number | 0
commaSeparator | 是否显示千分位分隔符 | Boolean | true
value | 显示的数值，可以是具体数字，也可以是数据字段 | Number/String | 0

### suffix
后缀

属性 | 说明 | 类型 | 默认值
--- | --- | --- | ---
content | 后缀的内容 | String | 元
fontSize | 文字大小，单位`px` | Number | 32
fontColor | 文字颜色 | String | #ffffff
bold | 是否加粗 | Boolean | false

## 联动事件  
无
