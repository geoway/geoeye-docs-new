## type属性
```
  "type": "button_group"
```
## paint属性

```
  "paint": {
    "tabs": [
      {
        "label": "汇总",
        "vars": {
          "moveFilter": "汇总"
        }
      },
      {
        "label": "重大点",
        "vars": {
          "moveFilter": "重大点"
        }
      },
      {
        "label": "督查点",
        "vars": {
          "moveFilter": "督查点"
        }
      }
    ],
    "style": {
      "orient": "horizontal",
      "fontSize": 14,
      "fontColor": '#ffffff',
      "borderColor": '#20bdec',
      "highlightColor": '#20bdec',   // 选中标签的字体颜色
      "activeBackground": 'rgba(21, 130, 162, 0.4)'  // 选中标签的背景颜色
    }
  }
```

## 联动事件  
该组件拥有一个联动事件，事件名称应在tabs中指定，可以指定每个tab对应的变量名和变量值，然后在events中配置对应的事件。每次点击按钮会触发该事件，可在其他组件中引用该事件的状态。
```
"events": {
  "moveFilter": {
    "alias": "moveFilter",     // 该事件的别名
    "default": "汇总"    // 该事件的初始值
  }
}
```
