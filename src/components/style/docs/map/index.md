## 概述

地图组件可支持基础底图、专题地图和外部服务，其中基础底图是通过 mapbox-gl 引擎渲染，地图样式应符合[Mapbox 地图样式规范](https://docs.mapbox.com/mapbox-gl-js/style-spec/)。专题地图的地图样式应符合[专题地图样式规范](http://121.41.205.54:1234/vmap-docs)。外部服务可以接入互联网瓦片服务，需要提供 xyz 模式的瓦片地址，瓦片大小可以手动指定，默认为 256。

## 类型

```
  "type": "map"
```

## paint 属性

```js
"paint": {
  "center": [120, 30],
  "zoom": 8,
  "bearing": 0,
  "pitch": 0,
  "map": {
    "type": "basemap",
    "style": "./data/mapStyle.json"
  }
}
```

| 属性    | 说明                                         | 类型   | 默认值        |
| ------- | -------------------------------------------- | ------ | ------------- |
| center  | 地图的中心点经纬度                           | Array  | [116.5, 39.5] |
| zoom    | 地图级别                                     | Number | 8             |
| bearing | 地图旋转角度                                 | Number | 0             |
| pitch   | 地图倾斜角度                                 | Number | 0             |
| map     | 地图的内容，支持基础底图、专题地图和外部服务 | Object | null          |

### 基础底图

类型为`basemap`，`style`为一个符合 Mapbox 样式规范的 json，或者指向该 json 的 url。

例如：

```js
{
  type: 'basemap',
  style: './data/mapStyle.json'
}
```

### 专题地图

类型为`vmap`，`style`为一个符合专题地图样式规范的 json，或者指向该 json 的 url。

例如：

```js
{
  type: 'vmap',
  style: './data/vmapStyle.json'
}
```

### 外部服务

类型为`service`，`style`为瓦片服务的地址，`tileSize`为瓦片大小，默认为 256。

例如：

```js
{
  type: 'service',
  style: 'https://t0.tianditu.gov.cn/DataServer?T=vec_c&x={x}&y={y}&l={z}&tk=c926ca77a933c367a1f5db4cae908348',
  tileSize: 256 // 可选
}
```
