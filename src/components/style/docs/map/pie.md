- 饼状图 

```
  {
    "id": "******",
    "name": "**",
    "type": "pie",
    "minzoom": 0,                                           // 图层的最小显示级别
    "maxzooom": 22,                                         // 图层的最大显示级别
    "visible": true,                                        // 图层是否可见
    "source": {                                             // 图层的数据，参考
      "url": "http://path/to/geojson",
      "type": "geojson"
    },
    "options": {
      "paint": {
        "series": [{
          "fields": ["f1", "f2", "f3"]            // 与饼状专题图中指定name字段和value字段不同，这里指定每一类的字段列表
        }]
      },
      "width": 100,
      "height": 100
    }
  }
```
