## 类型
```
  "type": "button"
```
## paint属性

```
  "paint": {
    "text": "按钮",
    "backgroundColor": "#5685f8",
    "fontSize": 18,
    "fontColor": "#ffffff",
    "width": 50,
    "height": 30
  }
```

## 联动事件  
该组件拥有一个联动事件，事件默认名称为**“clickEvent”**，每次点击按钮会触发该事件，可在其他组件中引用该事件的状态。如要修改该事件的名称和默认值，可以按以下方式配置：
```
"events": {
  "clickEvent": {
    "alias": "type",     // 该事件的别名
    "default": "确定"    // 该事件的初始值
  }
}
```
