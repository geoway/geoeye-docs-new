## 类型
```
  "type": "double_axis"
```
## paint属性

```js
  paint: {
    style: 'normal',
    series: [{...}],
    legend: {...},
    grid: {...},
    tooltip: {...},
    xAxis: {...},
    yAxis: [{...}]
  }
```

### series[]

数据系列，类型`Object Array`，双轴图的数据系列可以是柱状图，也可以是折线图/面积图，通过`type`属性进行区分，默认是柱状图

属性 | 说明 | 类型 | 默认值
--- | --- | --- | ---
type | 图表类型，可以是柱状图或者是折线图/面积图，可取值`bar` `line` | Enum String | bar
yAxisIndex | 图表对应的Y轴索引，`0`表示左轴，`1`表示右轴 | Enum Number | 0
... | ... | ... | ...

> 数据系列的其他属性可参考具体[柱状图](#/style/style-bar)或[折线图](#/style/style-line)的数据系列配置