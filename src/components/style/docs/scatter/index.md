## 类型
```
  "type": "scatter"
```
## paint属性

```js
  paint: {
    style: 'scatter',
    series: [{...}],
    legend: {...},
    grid: {...},
    tooltip: {...},
    xAxis: {...},
    yAxis: {...}
  }
```

### style

散点图的样式。类型`Enum String`，可取值`scatter`(普通散点图)、`bubble`(气泡图)，默认值`scatter`。

### series[]

数据系列，类型`Object Array`

属性 | 说明 | 类型 | 默认值
--- | --- | --- | ---
name | 系列名称 | String | ''
opacity | 透明度 | Number | 1
symbol | 符号，可取值`circle` `rect` `roundRect` `triangle` `diamond` `pin` `arrow` | Enum String | circle
symbolSize | 符号的大小，当图表类型为气泡图时，可以设为数组表示气泡的大小范围，例如`[5, 20]` | Number \| Array | 10
color | 符号的颜色 | String | #ff0000
showLabel | 显示数值标注 | Boolean | false
labelColor | 标注的颜色 | String | 跟随color
labelSize | 标注的大小 | Number | 14
labelPosition | 标注的位置，可取值`top` `left` `right` `bottom` | Enum String | top
field | 数据字段 | String | ''