## 类型
```
  "type": "pie"
```
## paint属性

```js
  paint: {
    colors: [],
    series: [{...}],
    legend: {...},
    tooltip: {...}
  }
```
### colors

颜色数组，饼图的颜色带，类型`Array`。默认为`null`，echarts默认颜色

### series[]

数据系列，类型`Object Array`


属性 | 说明 | 类型 | 默认值
--- | --- | --- | ---
name | 系列名称 | String | ''
radius | 饼图的半径，可以是百分比或具体的像素值，也可以是数组表示环形图 | String \| Number \| Array | 75%
center | 饼图的圆心的位置，数组第一个元素表示水平位置，第二个元素表示垂直位置，可以是百分比或具体的像素值 | Array | ['50%', '60%']
startAngle | 起始角度 | Number | 90
showLabel | 显示数值标注 | Boolean | false
showLabelLine | 显示标注线 | Boolean | false
labelColor | 标注的颜色 | String | #ffffff
labelSize | 标注的大小 | Number | 14
labelFormatter | 标注的内容，可参考[echarts文档](https://echarts.apache.org/zh/option.html#series-pie.label.formatter)，默认显示数值 | Enum String | {b}
rosePie | 玫瑰图 | Boolean | false
nameField | 名称字段 | String | ''
valueField | 数值字段 | String | ''