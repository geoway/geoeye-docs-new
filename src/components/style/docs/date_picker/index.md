## 类型
```
  "type": "date_picker"
```
## paint属性

```
  "paint": {
    borderColor: '#ffffff',
    borderWidth: 1,
    borderRadius: [4, 4, 4, 4],
    borderType: 'solid',
    fontColor: '#ffffff',
    fontSize: 16,
    format: 'yyyy-MM-dd'
  }
```

属性 | 说明 | 类型 | 默认值
--- | --- | --- | ---
borderColor | 边框的颜色 | String | #ffffff
borderWidth | 边框的宽度，单位`px` | Number | 1
borderRadius | 边框圆角的半径，单位`px`，可以是具体数值，也可以用数组分别设置四个角 | Number \| Array | [0, 0, 0, 0]
borderType | 边框的线型，可取值`solid`, `dashed`, `dotted` | Enum String | solid
fontColor | 文字颜色 | String | #ffffff
fontSize | 文字大小，单位`px` | Number | 16
format | 日期格式，年-`y`，月-`M`，日-`d` | String | yyyy-MM-dd

## 联动事件  
该组件拥有一个联动事件，事件默认名称为`date`，选中日期发生变化时会触发该事件，可在其他组件中引用该事件的状态。如要修改该事件的名称和默认值，可以按以下方式配置：
```
"events": {
  "date": {
    "alias": "myDate",     // 该事件的别名
    "default": "2020-02-26"    // 该事件的初始值
  }
}
```
