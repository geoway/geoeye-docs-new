## 类型
```
  "type": "funnel"
```
## paint属性

```js
  paint: {
    colors: [],
    series: [{...}],
    legend: {...},
    tooltip: {...}
  }
```

### colors

颜色数组，饼图的颜色带，类型`Array`。默认为`null`，echarts默认颜色

### series[]

数据系列，类型`Object Array`


属性 | 说明 | 类型 | 默认值
--- | --- | --- | ---
name | 系列名称 | String | ''
left | 左边距，可以是百分比或具体的像素值 | String \| Number | 10%
top | 上边距，可以是百分比或具体的像素值 | String \| Number | 80
width | 宽度，可以是百分比或具体的像素值 | String \| Number | 80%
height | 高度，可以是百分比或具体的像素值 | String \| Number | 80%
minSize | 最小宽度，可以是百分比或具体的像素值 | String \| Number | 0
maxSize | 最大宽度，可以是百分比或具体的像素值 | String \| Number | 100%
gap | 数据项之间的间隔 | Number | 0
inverse | 是否反向，`false`表示漏斗，`true`表示金字塔（倒立的漏斗） | Boolean | false
showLabel | 显示数值标注 | Boolean | false
showLabelLine | 显示标注线 | Boolean | false
labelColor | 标注的颜色 | String | #ffffff
labelSize | 标注的大小 | Number | 14
labelPosition | 标注的位置 | Enum String | right
labelFormatter | 标注的内容，可参考[echarts文档](https://echarts.apache.org/zh/option.html#series-funnel.label.formatter)，默认显示数值 | Enum String | {b}
nameField | 名称字段 | String | ''
valueField | 数值字段 | String | ''
