## 类型
```
  "type": "timer"
```
## paint属性

属性 | 说明 | 类型 | 默认值
--- | --- | --- | ---
fontSize | 文字大小，单位`px` | Number | 18
fontColor | 文字颜色 | String | #ffffff
fontFamily | 字体 | String | Microsoft Yahei
bold | 是否加粗 | Boolean | false
timeFormat | 显示的时间格式，年-`y`，月-`M`，日-`d`，时-`H`，分-`m`，秒-`s`，微秒-`S`，星期-`W` | String | yyyy-MM-dd HH:mm:ss.S  W
interval | 时间的更新间隔，单位`ms`，例如显示微秒的话应该将更新间隔设为100ms以下 | Number | 1000

## 联动事件  
无
