## 类型

```
  "type": "radar"
```
## paint属性

```js
  paint: {
    radar: {...},
    data: [{...}],
    legend: {...},
    tooltip: {...}
  }
```

### radar

雷达指示器，类型`Object`


属性 | 说明 | 类型 | 默认值
--- | --- | --- | ---
radius | 雷达图的半径，可以是百分比或具体的像素值 | String \| Number | 75%
center | 中心的位置，数组第一个元素表示水平位置，第二个元素表示垂直位置，可以是百分比或具体的像素值 | Array | ['50%', '60%']
shape | 雷达图的形状，可取值`polygon` `circle` | Enum String | polygon
max | 指示器的最大值 | Number | 自动
lineColor | 指示线的颜色 | String | #ffffff
lineWidth | 指示线的宽度 | Number | 1
areaColor | 雷达面的颜色 | String | rgba(114, 172, 209, 0.2)
labelColor | 标注的颜色 | String | #ffffff
labelSize | 标注的大小 | Number | 14
field | 名称字段 | String | ''

### data[]

数据系列，类型`Object Array`


属性 | 说明 | 类型 | 默认值
--- | --- | --- | ---
name | 系列名称 | String | ''
showNode | 线上显示小圆点 | Boolean | true
lineWidth | 线宽 | Number | 2
lineColor | 线的颜色 | String | #ff0000
lineStyle | 线型，可取值`solid` `dashed` `dotted` | Enum String | solid
fillColor | 面的颜色 | String | null
fillOpacity | 面的透明度 | Number | 1
showLabel | 显示数值标注 | Boolean | false
labelColor | 标注的颜色 | String | 跟随lineColor
labelSize | 标注的大小 | Number | 14
labelPosition | 标注的位置，可取值`top` `left` `right` `bottom` | Enum String | top
field | 数据字段 | String | ''
