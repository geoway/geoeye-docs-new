## 类型
```
  "type": "legend"
```
## paint属性

属性 | 说明 | 类型 | 默认值
--- | --- | --- | ---
mapId | 图例对应的地图组件的Id | String | ''
backgroundColor | 背景颜色 | String | #033c63
borderColor | 边框颜色 | String | #00ccff
borderWidth | 边框宽度 | Number | 1
borderRadius | 边框半径，可以是具体的数值，也可以是数组分别表示四个角 | Number\|String | 5
fontColor | 文字颜色 | String | #ffffff
fontSize | 文字大小，仅提供小(`small`)、中(`middle`)、大(`large`)三种大小供选择 | Enum String | small
itemWidth | 图例色块的宽度 | Number | 25
itemHeight | 图例色块的高度 | Number | 15

## 联动事件  
无
