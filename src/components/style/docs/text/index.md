## 类型
```
  "type": "text"
```
## paint属性

属性 | 说明 | 类型 | 默认值
--- | --- | --- | ---
text | 显示的文字内容，如果未指定`textField`，则直接显示该属性值 | String | -
textField | 显示的字段，必须设置了数据源才能使用该属性，用于显示数据源中的某一个字段值。设置了该属性会忽略`text`属性 | String | value
fontSize | 文字大小，单位`px` | Number | 18
fontColor | 文字颜色 | String | #ffffff
fontFamily | 字体 | String | Microsoft Yahei
bold | 是否加粗 | Boolean | false
italic | 是否显示为斜体 | Boolean | false
letterSpacing | 文字之间的间隔，单位为`em` | Number | 0
textAlign | 对齐方式，可以是`left`、`center`或`right` | String | center
textWrap | 是否允许换行 | Boolean | false
lineHeight | 行高，单位`em`，仅在textWrap为`true`时有效 | Number | 1.5
textIndent | 首行缩进，单位`em`，仅在textWrap为`true`时有效 | Number | 0
showBorder | 是否显示边框 | Boolean | false
borderWidth | 边框的宽度，单位`px` | Number | 1
borderColor | 边框的颜色 | String | #0099ff
borderType | 边框的样式，可以是`solid`、`dashed`或`dotted` | Enum String | solid
borderRadius | 边框圆角的半径，单位`px` | Number Array | [0, 0, 0, 0]
link | 超链接，仅对单行文本（`textWrap=false`）有效 | String | ""
target | 超链接打开的位置，可取值`_blank`、`_self`，，仅对单行文本（`textWrap=false`）有效 | Enum String | \_blank

## 联动事件  
无

**示例**  
<a href="./examples/#/title" target="_blank">查看示例</a>