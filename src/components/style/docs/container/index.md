## 类型
```
  "type": "container"
```
## paint属性

```
  "paint": {
    "border": {
      "image": ""
      "color": "#ffffff",
      "type": "solid",
      "width": 12,
      "radius": [0, 0, 0, 0],  // 四个角的边框半径
    },
    "background": {
      "image": "",
      "color": "rgba(0, 0, 0, 0.5)",
      "repeat": "norepeat_center",
      "gradientDirection": "top",
    },
    "boxShadow": 'none',
    "components": []
  }
```

<table>
  <tr>
    <th>属性</th>
    <th>子属性</th>
    <th style="width: 50%;">说明</th>
    <th>类型</th>
    <th>默认值</th>  
  </tr >
  <tr >
    <td rowspan="5">border</td>
    <td>image</td>
    <td>边框图片的地址</td>
    <td>String</td>
    <td>无</td>
  </tr>
  <tr>
    <td>color</td>
    <td>边框的颜色</td>
    <td>String</td>
    <td>#ffffff</td>
  </tr>
  <tr>
    <td>type</td>
    <td>边框的线型，可取值`solid`, `dashed`, `dotted`</td>
    <td>Enum String</td>
    <td>solid</td>
  </tr>
  <tr>
    <td>width</td>
    <td>边框的宽度，单位`px`</td>
    <td>Number</td>
    <td>0</td>
  </tr>
  <tr>
    <td>radius</td>
    <td>边框圆角的半径，单位`px`，可以是具体数值，也可以用数组分别设置四个角</td>
    <td>Number | Array</td>
    <td>[0, 0, 0, 0]</td>
  </tr>
  <tr>
    <td rowspan="4">background</td>
    <td>image</td>
    <td>背景图片的地址</td>
    <td>String</td>
    <td>无</td>
  </tr>
  <tr>
    <td>color</td>
    <td>背景的颜色，可以用数组指定多个颜色来设置渐变</td>
    <td>String | Array</td>
    <td>rgba(0, 0, 0, 0.5)</td>
  </tr>
  <tr>
    <td>repeat</td>
    <td>背景图片的展示方式，可取值：`norepeat_full`（不重复，拉伸铺满）、`norepeat_center`（不重复，居中）、`repeat`（水平垂直重复）、`repeat_x`（水平重复）、`repeat_y`（垂直重复）</td>
    <td>Enum String</td>
    <td>norepeat_full</td>
  </tr>
  <tr>
    <td>gradientDirection</td>
    <td>渐变方向，仅当<code>color</code>为数组的时候有效，可取值：top、bottom、center、left、right</td>
    <td>Enum String</td>
    <td>top</td>
  </tr>
  <tr>
    <td>boxShadow</td>
    <td>-</td>
    <td>阴影设置，一般用来设置发光效果</td>
    <td>String</td>
    <td>none</td>
  </tr>
  <tr>
    <td>components</td>
    <td>-</td>
    <td>容器中的子组件列表</td>
    <td>Array</td>
    <td>[]</td>
  </tr>
</table>

## 联动事件  
无
