## 类型
```
  "type": "image"
```
## paint属性

属性 | 说明 | 类型 | 默认值
--- | --- | --- | ---
src | 图片的地址 | String | -
alt | 图片请求失败时显示的文字 | String | -
href | 图片的超链接，可以通过点击图片跳转到其他页面 | String | javascript:void(0)
target | 超链接打开的位置，可以是`_blank`,`_self`,`_top`,`_parent` | Enum String | \_blank

## 联动事件  
无
