## 类型
```
  "type": "progress_bar"
```
## paint属性

```
  "paint": {
    bar: {
      align: 'left',            // 从哪里开始，left或right
      backgroundColor: '#f3f3f3',
      color: '#4b8cf0',
      backgroundImage: '',        // 用图片替代backgroundColor
      image: '',                  // 用图片替代color
      endShape: 'round',
      strokeWidth: 10,
      leftGap: 10,       // 距离左边的距离
      rightGap: 10       // 距离右边的距离
    },
    label: {
      content: '{percent}',      // {percent}为百分比，{value}为数值，{total}为总数，支持HTML
      decimalDigits: 2,          // 百分比保留几位小数
      position: 'right',         // 标注的位置，可选值："left"、"right"
      color: '#ffffff',
      fontSize: 16
    },
    data: {
      "value": 20,                     // 值，可以是具体的数值，也可以是字段名
      "total": 100                     // 总数，可以是具体的值，也可以是字段名
    }
  }
```

## 联动事件  
无
