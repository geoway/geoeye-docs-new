## 类型
```
  "type": "word_cloud"
```
>  注意： 词云图需要额外的插件，应该在引入echarts库之后引入词云图的插件：
```html
  <script src='./libs/echarts.min.js'></script>
  <script src="./libs/echarts-wordcloud.min.js"></script>
```

## paint属性

```js
paint: {
  series: { ... }
}
```

### series
系列设置

属性 | 说明 | 类型 | 默认值
--- | --- | --- | ---
name | 名称，主要用于tooltip显示 | String | ''
minSize | 最小字号 | Number | 12
maxSize | 最大字号 | Number | 60
colors | 颜色带 | Array | -
gap | 文字之间的间距 | Number | 8
shape | 词云的形状，可以指定任意图片的url | String | ''
nameField | 名称字段，用于显示的主题词 | String | ''
valueField | 权重字段 | String | ''

## 联动事件  
无
