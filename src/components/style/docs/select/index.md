## 类型

选择框的内容来自source属性中的第一个条目(sourceTable[0])
```
  "type": "select"
```
## paint属性

```
  "paint": {
    "data": [],
    "fontColor": "#ffffff",
    "fontSize": 14,
    "backgroundColor": "transparent",
    "gradientDirection": "top",
    "borderColor": "#6ba4fd",
    "borderRadius": 5,
    "optionColor": "#1f4375"
  }
```
属性 | 说明 | 类型 | 默认值
--- | --- | --- | ---
data | 下拉框的选项列表，包含`name`和`value`两个属性，例如`[{name: '中国', value: 'China'}]` | Array | []
fontColor | 文字颜色 | String | #ffffff
fontSize | 文字大小，单位`px` | Number | 14
backgroundColor | 背景的颜色，可以是颜色数组表示渐变色 | String | transparent
gradientDirection | 渐变颜色的方向，可取值`top` `bottom` `left` `right` `center` | Enum String | top
borderColor | 边框的颜色 | String | #6ba4fd
borderRadius | 边框圆角的半径，单位`px` | Number | 5
optionColor | 选项高亮的颜色 | String | #1f4375

## 联动事件  
该组件拥有一个联动事件，事件默认名称为**“selectEvent”**，每次点击按钮会触发该事件，可在其他组件中引用该事件的状态。如要修改该事件的名称和默认值，可以按以下方式配置：
```
"events": {
  "selectEvent": {
    "alias": "areaCode",           // 别名，可以自定义，表示该选择框绑定的变量名
    "default": "330000"            // 默认值
  }
}
```
