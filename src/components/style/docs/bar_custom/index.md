## 类型

```
  "type": "bar_custom"
```
## paint属性

```js
  paint: {
    series: [{...}],
    legend: {...},
    grid: {...},
    tooltip: {...},
    xAxis: {...},
    yAxis: {...}
  }
```

### series[]

数据系列，类型`Object Array`


属性 | 说明 | 类型 | 默认值
--- | --- | --- | ---
name | 系列名称 | String | ''
symbol | 自定义符号，包括内置符号或自定义符号，内置符号`hill` `triangle` `diamond`，自定义符号用`image://imageUrl`指定自定义图片 | String | hill
symbolRepeat | 符号是否叠加 | Boolean | false
symbolSize | 符号的大小，默认不设置，符号会根据数值大小进行拉伸，一般在`symbolRepeat`设为`true`时，可设置固定的符号大小来进行叠加 | Array | null
symbolInterval | 符号叠加的间隔，仅在`symbolRepeat`为true时有效 | Number | 0
barWidth | 柱子的宽度 | Number | 100
barColor | 柱子的颜色，可以用多个颜色的数组表示渐变色 | String \| Array | ''
gradientDirection | 渐变颜色的方向，可取值`top` `bottom` `left` `right` `center` | Enum String | top
opacity | 透明度 | Number | 0.5
showLabel | 显示数值标注 | Boolean | false
labelColor | 标注的颜色 | String | 跟随barColor
labelSize | 标注的大小 | Number | 14
labelPosition | 标注的位置，可取值`top` `left` `right` `bottom` `inside` | Enum String | top
field | 数据字段 | String | ''
