# 组件配置

> 可视化组件配置定义了组成大屏的各种可视化组件的数据源、布局、样式等信息。  
> 组成大屏的可视化组件以数组方式配置，数组中位置决定了组件的渲染次序和叠加关系。在数组头部的组件先渲染，位置靠底层；在数组尾部的组件后渲染，位置靠顶层。  
> 可视化组件分为图表组件、地图组件和多媒体组件，组件的分类信息并不体现在配置中，只是用来便于理解和组织。


## 基本结构

可视化组件配置的基本结构如下，包括组件的ID、类型、数据源、布局和样式信息的配置。

```
"components": [
  {
    "id": "abcde",
    "type": "bar",
    "source": {...},
    "layout": {...},
    "paint": {...},
    "events": {...}
  }
]
```

### id

*类型：String，必选*

组件的唯一标识码，不得与已有组件重复。


### type

*类型：Enum，必选*

组件的类型，可选的类型值参见各组件的配置说明。


### source

*类型：Object*

组件的数据源，支持静态数据源和API数据源，详细地配置项见[组件数据源](./#/style/style-source)


### layout

*类型：Object，必选*

组件的布局，包括组件的大小、位置、是否可见的设置，详细的配置项[组件布局](./#/style/style-layout)。


### paint

*类型：Object*

组件的样式，包括组件元素的字体、颜色、线条等配置，各组件的样式配置项各不相同，详细的配置项点击各组件查看详情。

### events

*类型：Object*

组件联动相关的配置，只有部分组件具有联动事件，例如[按钮](./#/style/style-button)， 详细的配置项参见[组件联动](./#/style/style-connect)。


