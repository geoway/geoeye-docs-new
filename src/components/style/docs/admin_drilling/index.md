# 行政区划钻取

## 概述

主要是分级设色图按照行政区进行上钻下取，使用者只需要根据行政区划ID挂上数据，并进行简单配置，就可以实现指定行政区级别的上钻下取。

## 使用方法

获取自定义组件配置实例：

```js
new AdminDrilling(mapId:String, mapType:String, options:Object)
```

示例：

```js
import { AdminDrilling } from 'admin-drilling'

let config = {...}  // 大屏的配置
var screen = new Screen(config, {  // 初始化大屏
  container: 'geoeye-box'
})
let options = {...}  // 行政区划图的配置项
screen.addComponent(new AdminDrilling('map001', 'visual_map', options))
```
## 参数说明

`mapId`

地图的id，可以指定为任意字符串，不能与其他可视化组件的id重复

`mapType`

地图的类型，目前仅支持行政区划图分级设色，因此值只能为`'visual_map'`

`options`

地图的配置选项，属性结构与[组件配置规范](#/style/style-component)一致，其中`source`属性应该指定挂载到地图上的数据。

这里仅列出其中`paint`属性的结构：

```
  "paint": {
    "map": {
      "mapBasePath": "./data/geojson",
      "areaId": "156",
      "enableDrill": false,
      "showLabel": true
    },
    "data": {
      "nameByCode": true,
      "nameField": "pac",
      "valueField": "confirmedTotal"
    },
    "rangeBar": {
      "min": 0,
      "max": 1010,
      "topText": "高",
      "bottomText": "低"
    }
  }
```

- map

属性 | 说明 | 类型 | 默认值
--- | --- | --- | ---
mapBasePath | 行政区划数据的地址，GeoJSON格式，可以点击[这里](./data/geojson.zip)下载全国行政区划数据 | String | -
areaId | 需要显示的行政区的Id，例如全国行政区划图id为156 | String | 156
enableDrill | 是否启用钻取 | Boolean | false
showLabel | 是否显示地图标注 | Boolean | false

- data

属性 | 说明 | 类型 | 默认值
--- | --- | --- | ---
nameField | 行政区划的名称字段，由该字段值来与行政区进行挂接 | String | ''
valueField | 数据字段，由该字段值来对行政区进行分级设色 | String | ''
nameByCode | 是否根据行政区Id来匹配行政区名称。如果`nameField`获得的不是名称而是行政区id，则应该将该字段设为true，否则将直接将nameField作为名称 | Boolean | true

- rangeBar

配置地图图例

属性 | 说明 | 类型 | 默认值
--- | --- | --- | ---
min | 最小值 | Number | 0
max | 最大值 | Number | 数据中的最大值
topText | 图例顶部显示的文字 | String | 高
bottomText | 图例底部显示的文字 | String | 低

## 示例

点击[查看示例](#/cases/drilling)
