## 类型
```
  "type": "circle"
```
## paint属性

```
  "paint": {
    "strokeWidth": 10,               // 环的宽度
    "gradientDirection": 'top',      // 渐变色的方向，仅对backgroundColor和color设置渐变色有效
    "backgroundColor": '#f3f3f3',    // 环的背景颜色，可以是颜色值或者颜色数组，颜色数组会按照渐变色显示
    "color": '#4b8cf0',              // 环进度颜色，可以是颜色值或者颜色数组，颜色数组会按照渐变色显示
    endShape: 'butt',                // 圆角还是方角
    startAngle: 0,                   // 开始角度，正上方为0度，顺时针旋转
    endAngle: 360,                   // 结束角度
    showLabel: true,                 // 是否显示标注文字（百分比）
    labelColor: "#4b8cf0",           // 标注颜色
    labelSize: 24,                   // 标注文字大小
    decimalDigits: 0,                // 小数点位数
    "value": 20,                     // 值，可以是具体的数值，也可以是字段名
    "total": 100                     // 总数，可以是具体的值，也可以是字段名
  }
```

## 联动事件  
无
