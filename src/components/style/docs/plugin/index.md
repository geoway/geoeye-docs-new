# 自定义组件

> 可视化的需求千变万化，无论多丰富的组件也无法满足所有的可视化需求，对于可视化引擎内置组件无法满足需求的情况，可以通过自定义组件的形式对组件进行扩展。

> 引擎目前已有的自定义组件可以在“【组件】——【自定义组件】”中进行查看，也可以通过本文介绍的方法开发自己的组件。

### 组件开发

自定义组件的配置也需要遵循[组件配置](#/style/style-component)规范，组件布局、组件数据源、组件联动均与配置规范完全一致，一般不需要单独处理。

自定义组件需要注意以下几点：

- 自定义组件的`type`属性值只能为`custom`
- 自定义组件的`paint`属性由组件开发者自定义
- 开发者需要提供一个`render`函数用于组件绘制，`render`函数的参数包含绘制组件的必要属性
- 如果`paint`属性中有部分属性值需要与其他组件进行联动，依然可以通过`{{...}}`占位符来代替，开发者一般不需要额外处理，当占位符中的变量值发生更新时，引擎会自动调用`render`函数对组件进行重绘
- `render`函数可能会被多次调用来重绘组件，但是引擎重绘时不会主动销毁原来的组件，开发者需要在`render`函数自行处理组件更新

**render函数的参数**

`render(component, paint)`

render函数包含两个参数，第一个参数是当前组件的实例(`this`)，第二个参数是替换了占位符以后的paint属性。

组件实例包含以下常用属性：

| 属性名 | 类型 | 说明 |
| ----- | ----- | ----- |
| layer | Object | 组件的完整配置 |
| originalData | Array/Object | 通过`source`配置请求到的原始数据 |
| container | HTMLElement | 组件的DOM容器 |

### 示例

一个完整的自定义组件配置如下：

```js
{
  id: 'aaa001',
  type: 'custom',
  source: {},
  layout: {
    width: 80,
    height: 30,
    offsetX: 260,
    offsetY: 150,
    visible: true
  },
  paint: {
    tips: '审核成功',
    type: 'success'
  },
  render(component, paint) { // 通过render函数渲染自定义组件
    let {container} = component
    container.innerHTML = ''
    let div = document.createElement('div')
    div.textContent = paint.tips
    let color = paint.type === 'error' ? '#e73e2d' : (paint.type === 'warning' ? '#ed9720' : '#5bbf6b')
    div.style.padding = '5px 10px'
    div.style.backgroundColor = color
    div.style.borderRadius = '4px'
    div.style.color = '#ffffff'
    div.style.textAlign = 'center'
    container.appendChild(div)
  }
}
```

详细示例可以查看[自定义组件](#/cases/custom)