## 类型
```
  "type": "timeline"
```
## paint属性


```
  "paint": {
    "autoPlay": false,      // 是否自动播放
    "playInterval": 1000,   // 自动播放间隔
    "orient": "horizontal", // 时间轴的方向：水平（horizontal）、垂直（vertical），默认为"horizontal"
    "lineColor": '#aaaaaa',
    "lineWidth": 2,
    "nodeColor": '#aaaaaa',
    "showLabel" : true,      // 是否显示文字
    "labelPosition": 'bottom',  // 文字的位置："left"(只对垂直方向有效)、"right"(只对垂直方向有效)、"bottom"(只对水平方向有效)、"top"(只对水平方向有效)，默认为"bottom"
    "labelColor": '#ffffff',
    "labelSize": 12,
    "field": ""        // 时间轴的数据字段
  }
```

## 联动事件  
该组件拥有一个联动事件，事件默认名称为**“year”**，时间轴的每次更新会触发该事件，可在其他组件中引用该事件的状态。如要修改该事件的名称和默认值，可以按以下方式配置：
```
"events": {
  "year": {
    "alias": "region",     // 该事件的别名
    "default": "湖北省"    // 该事件的初始值
  }
}
```
