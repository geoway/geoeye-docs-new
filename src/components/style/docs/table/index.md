## 类型

```
  "type": "table"
```
## paint属性

```js
paint: {
  head: {...},
  cols: [{...}],
  row: {...},
  autoplay: false
}
```

<table>
  <tr>
    <th>属性</th>
    <th>子属性</th>
    <th style="width: 50%;">说明</th>
    <th>类型</th>
    <th>默认值</th>  
  </tr >
  <tr >
    <td rowspan="7">head</td>
    <td>height</td>
    <td>表头的高度，单位为<code>px</code></td>
    <td>Number</td>
    <td>40</td>
  </tr>
  <tr>
    <td>backgroundColor</td>
    <td>表头背景色</td>
    <td>String</td>
    <td>rgb(63, 177, 227)</td>
  </tr>
  <tr>
    <td>fontFamily</td>
    <td>表头的字体</td>
    <td>String</td>
    <td>Microsoft YaHei</td>
  </tr>
  <tr>
    <td>fontSize</td>
    <td>表头文字大小，单位`px`</td>
    <td>Number</td>
    <td>14</td>
  </tr>
  <tr>
    <td>fontColor</td>
    <td>表头文字颜色</td>
    <td>String</td>
    <td>#ffffff</td>
  </tr>
  <tr>
    <td>bold</td>
    <td>是否加粗</td>
    <td>Boolean</td>
    <td>true</td>
  </tr>
  <tr>
    <td>italic</td>
    <td>是否显示斜体</td>
    <td>Boolean</td>
    <td>false</td>
  </tr>
  <tr>
    <td rowspan="4">cols[]</td>
    <td>label</td>
    <td>列名</td>
    <td>String</td>
    <td>无</td>
  </tr>
  <tr>
    <td>field</td>
    <td>该列对应的数据字段</td>
    <td>String</td>
    <td>无</td>
  </tr>
  <tr>
    <td>colWidth</td>
    <td>列宽，如果不设或者设为0，则表示自动列宽</td>
    <td>Number</td>
    <td>0</td>
  </tr>
  <tr>
    <td>sort</td>
    <td>是否排序，可取值：none、desc、asc</td>
    <td>Enum String</td>
    <td>none</td>
  </tr>
  <tr >
    <td rowspan="9">row</td>
    <td>height</td>
    <td>行高，单位为<code>px</code></td>
    <td>Number</td>
    <td>40</td>
  </tr>
  <tr>
    <td>count</td>
    <td>显示行数，不设置或设为0表示显示所有行。如果设置了行数会忽略行高</td>
    <td>Number</td>
    <td>0</td>
  </tr>
  <tr>
    <td>oddColor</td>
    <td>奇数行颜色</td>
    <td>String</td>
    <td>rgba(0, 0, 0, 0.4)</td>
  </tr>
  <tr>
    <td>evenColor</td>
    <td>偶数行颜色</td>
    <td>String</td>
    <td>rgba(63, 177, 227, 0.4)</td>
  </tr>
  <tr>
    <td>fontFamily</td>
    <td>字体</td>
    <td>String</td>
    <td>Microsoft YaHei</td>
  </tr>
  <tr>
    <td>fontSize</td>
    <td>文字大小，单位`px`</td>
    <td>Number</td>
    <td>14</td>
  </tr>
  <tr>
    <td>fontColor</td>
    <td>文字颜色</td>
    <td>String</td>
    <td>#ffffff</td>
  </tr>
  <tr>
    <td>bold</td>
    <td>是否加粗</td>
    <td>Boolean</td>
    <td>true</td>
  </tr>
  <tr>
    <td>italic</td>
    <td>是否显示斜体</td>
    <td>Boolean</td>
    <td>false</td>
  </tr>
  <tr>
    <td>autoplay</td>
    <td>-</td>
    <td>是否自动播放</td>
    <td>Boolean</td>
    <td>false</td>
  </tr>
</table>
