## 类型
```
  "type": "metric"
```
## paint属性

```
  "paint": {
    common: {
      fontFamily: 'Microsoft YaHei',
      textAlign: 'left',
      orient: 'horizontal'  // 排列方式：horizontal（水平排列）、vertical（垂直排列）
    },
    prefix: {
      content: '￥',
      fontColor: '#ffffff',
      fontSize: 48,
      bold: false,   // 是否加粗
      lineHeight: 1
    },
    number: {
      fontColor: '#fff000',
      fontSize: 48,
      bold: false,
      decimalDigits: 0,           // 保留小数位数
      commaSeparator: true,        // 是否显示千分位分隔符
      value: 'value',
      lineHeight: 1
    },
    suffix: {
      content: '元',
      fontColor: '#ffffff',
      fontSize: 32,
      bold: false,
      lineHeight: 1
    }
  }
```
### common
通用属性

属性 | 说明 | 类型 | 默认值
--- | --- | --- | ---
fontFamily | 字体 | String | Microsoft Yahei
textAlign | 对齐方式，可以是`left`、`center`或`right` | String | left
orient | 排列方式，可取值`horizontal`、`vertical` | Enum String | horizontal

### prefix
前缀

属性 | 说明 | 类型 | 默认值
--- | --- | --- | ---
content | 前缀的内容 | String | ￥
fontSize | 文字大小，单位`px` | Number | 48
fontColor | 文字颜色 | String | #ffffff
bold | 是否加粗 | Boolean | false
lineHeight | 行高，单位`em` | Number | 1

### number
数值

属性 | 说明 | 类型 | 默认值
--- | --- | --- | ---
fontSize | 文字大小，单位`px` | Number | 48
fontColor | 文字颜色 | String | #fff000
bold | 是否加粗 | Boolean | false
decimalDigits | 小数点后保留的位数 | Number | 0
commaSeparator | 是否显示千分位分隔符 | Boolean | true
value | 显示的数值，可以是具体数字，也可以是数据字段 | Number/String | value
lineHeight | 行高，单位`em` | Number | 1

### suffix
后缀

属性 | 说明 | 类型 | 默认值
--- | --- | --- | ---
content | 后缀的内容 | String | 元
fontSize | 文字大小，单位`px` | Number | 32
fontColor | 文字颜色 | String | #ffffff
bold | 是否加粗 | Boolean | false
lineHeight | 行高，单位`em` | Number | 1

## 联动事件  
无
