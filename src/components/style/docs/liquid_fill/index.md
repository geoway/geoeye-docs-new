## 类型
```
  "type": "liquid_fill"
```
>  注意： 水球图需要额外的插件，应该在引入echarts库之后引入水球图的插件：
```html
  <script src='./libs/echarts.min.js'></script>
  <script src="./libs/echarts-liquidfill.min.js"></script>
```

## paint属性

```
  "paint": {
    color: '#4b8cf0',    // 颜色
    labelColor: undefined,  // 显示百分比的颜色，默认与color相同
    labelSize: undefined,   // 显示百分比的大小，默认自适应
    value: 20,              // 数值，可以是具体的值，或者字段名
    total: 100              // 总数，可以是具体的值，或者字段名
  }
```

## 联动事件  
无
