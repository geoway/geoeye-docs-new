## 类型
```
  "type": "tab"
```
## paint属性

```
  "paint": {
    "tabs": [
      {
        "label": "",      // tab组件上显示的名字
        "charts": []      // 控制的组件id列表
      },
      {
        "label": "",
        "charts": []
      },
      {
        "label": "",
        "charts": []
      },
      ……
    ],
    "style": {
      "fontSize": 14,
      "fontColor": '#ffffff',
      "borderColor": '#20bdec',
      "activeFontColor": '#20bdec',  // 选中标签的字体颜色
      "activeBackground": 'rgba(21, 130, 162, 0.4)'  // 选中标签的背景颜色
    }
  }
```

## 联动事件  
无
