## 类型
```
  "type": "video"
```
## paint属性

属性 | 说明 | 类型 | 默认值
--- | --- | --- | ---
src | 视频的地址 | String | -
autoplay | 自动播放 | Boolean | true
controls | 是否显示控制条 | Boolean | true
muted | 静音 | Boolean | false
loop | 循环播放 | Boolean | true

## 联动事件  
无
