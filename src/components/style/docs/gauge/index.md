## 类型
```
  "type": "gauge"
```
## paint属性

```
  "paint": {
    tooltip: {
      show: true,
      formatter: undefined,
      textColor: '#ffffff',
      textSize: 14
    },
    "series": [{
      "name": "",
      "nameField": "", // 名称字段，如果不设置则直接用name属性
      "valueField": "",
      "radius": '90%',
      "center": ['50%', '55%'],
      "min": 0,
      "max": 100,
      "width": 10,   // 仪表盘的宽度
      "splitNumber": 10, // 分段数
      "colors": undefined,  // 色带
      "showLabel": true,   // 是否显示标注
      "labelColor": '#ffffff', 
      "labelSize": 14,
      "showTitle": true,   // 是否显示标题
      "titleColor": '#ffffff',
      "titleSize": 24,
      "titlePosition": [0, '-40%'], // 标题位置，相对于仪表中心点，可以是具体像素值或百分比
      "showValue": true,   // 是否显示数值
      "valueColor": '#fff000',
      "valueSize": 16,
      "valuePosition": [0, '40%'],   // 数值的位置，相对于仪表中心点，可以是具体像素值或百分比
      "valueFormatter": undefined,   // 数值的格式，例如"{value}%"
      "showTick": true, // 是否显示刻度
      "startAngle": 225,
      "endAngle": -45
    }]
  }
```

## 联动事件 
无
