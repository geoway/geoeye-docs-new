**type属性**
```
  "type": "line"
```
**paint属性**

```js
  paint: {
    style: 'normal',
    series: [{...}],
    legend: {...},
    grid: {...},
    tooltip: {...},
    xAxis: {...},
    yAxis: {...}
  }
```

### style

折线图的样式。类型`Enum String`，可取值`normal`(普通折线图)、`overlay`(堆叠折线图)。面积图可通过设置数据系列中的`areaColor`实现。

### series[]

数据系列，类型`Object Array`


属性 | 说明 | 类型 | 默认值
--- | --- | --- | ---
name | 系列名称 | String | ''
lineWidth | 线宽 | Number | 2
lineColor | 线的颜色 | String | #ff0000
lineType | 线型，可取值`solid` `dashed` `dotted` | Enum String | solid
symbol | 线上折点的符号，可取值`circle` `rect` `roundRect` `triangle` `diamond` `pin` `arrow` | Enum String | circle
symbolSize | 线上折点的大小 | Number | 5
areaColor | 面的颜色，用于渲染面积图，也可以是颜色数组用来表示渐变色 | String \| Array | null
areaOpacity | 面的透明度 | Number | 0.8
gradientDirection | 渐变颜色的方向，可取值`top` `bottom` `left` `right` `center` | Enum String | top
showLabel | 显示数值标注 | Boolean | false
labelColor | 标注的颜色 | String | 跟随lineColor
labelSize | 标注的大小 | Number | 14
labelPosition | 标注的位置，可取值`top` `left` `right` `bottom` | Enum String | top
smooth | 平滑线 | Boolean | false
field | 数据字段 | String | ''