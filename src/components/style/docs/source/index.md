# 组件数据源

组件的数据源有两种：静态数据源和API数据源。静态数据源直接将数据内联到样式文件中，适用于少量的静态数据；API数据源需要通过网络请求数据，适用于大量的动态数据。

## 基本结构

数据源配置示例如下：

```
"source": {
  "type": "table || geojson",
  "data": [], // 静态数据
  "url": "http://host.com/data.csv",   // API数据
  "proxy": true,    // 是否代理，仅对url有效
  "frequency": 5,    // 重复请求，仅对url有效
  "transform": [
    {
      "type": "filter",
      "condition": [">", "population", 5]
    },
    {
      "type": "aggregate",
      "fields": [],        // 统计字段集
      "operations": [],    // 统计操作集
      "as": [],            // 存储字段集
      "groupBy": []        // 分组字段集
    },
    {
      "type": "sort",
      "fields": ["year"],   // 根据指定的字段集进行排序，与lodash的sortBy行为一致
      "orders": ["desc"]    // 默认为 asc，desc 则为逆序
    }, 
    {
      "type": "map"
    }
  ]
}
```

各属性项说明如下：

### type

*类型：Enum，可选值：table、geojson，默认：table*

数据源的类型，有如下类型：

- table：表格数据；
- geojson: 地理数据。


### data

*类型：Array，默认：[]*

表示静态数据。

### url

*类型：String*

表示通过API请求数据。

### proxy（暂未实现）

*类型：Boolean，默认：false*

是否通过代理请求（仅对API请求数据有效）。

### frequency

*类型：Number，默认：0*

表示重复请求数据的频率，单位s，仅对api数据有效，适用于实时数据或更新比较频繁的数据。默认为0，表示不重复请求。


### transform

*类型：Array，可选*

数据转换配置项，用于将数据进行类型转换、过滤、排序、聚合，转化为可视化组件所需的数据格式，示例如下：

**过滤**
```
  {
    "type": "filter",
    "condition": ["all, [">", "population", 5], ["<=", "gdp", 200]],
    "callback": function(n) { // (暂未实现)
      return n > 5
    }
  }
```
可以用数组指定过滤条件，也可以通过回调函数自定义过滤规则。<a href="#/cases/filter" target="_blank">查看示例</a>  
目前支持以下几种条件过滤：  
1、字段是否存在  
`["has", "field"]` => field字段存在  
`["!has", "field"]` => field字段不存在  
2、字段值的比较  
`["==", "field", "value"]`  => field字段的值等于value  
`["!=", "field", "value"]`  => field字段的值不等于value  
`[">", "field", "value"]`  => field字段的值大于value  
`[">=", "field", "value"]`  => field字段的值大于等于value  
`["<", "field", "value"]`  => field字段的值小于value  
`["<=", "field", "value"]`  => field字段的值小于等于value  
3、字段值是否为已知值  
`["in", "field", "value1", "value2", "value3", "···"]`  =>  field字段的值为[value1, value2, value3, ···]中的一个  
`["!in", "field", "value1", "value2", "value3", "···"]`  =>  field字段的值不是[value1, value2, value3, ···]中的一个   
4、多个条件组合过滤  
`["all", "f1", "f2", "···"]`  =>  逻辑且，f1且f2且···  
`["any", "f1", "f2", "···"]`  =>  逻辑或，f1或f2或···  
`["none", "f1", "f2", "···"]`  =>  逻辑非，非f1且非f2且非···  
> 补充：过滤条件中所有的字段(field)应该为一个字符串，过滤条件中所有的值(value)可以是具体的值，也可以来自于一个表达式，目前支持以下表达式：  
① 某个字段的值： `["get", "field"]`  
② 求和表达式：   `["+", "value1", "value2"]`  
③ 求差表达式：   `["-", "value1", "value2"]`  
④ 求积表达式：   `["*", "value1", "value2"]`  
⑤ 求商表达式：   `["/", "value1", "value2"]`  
⑥ 求余表达式：   `["%", "value1", "value2"]`  

**聚合**
```
  {
    "type": "aggregate",
    "fields": [],        // 统计字段集
    "operations": [],    // 统计操作集
    "as": [],            // 存储字段集
    "groupBy": "pop"        // 分组字段集
  }
```
fields/operations/as这三个数组元素一一对应：“对某个字段field进行某种统计操作operation结果存储在某个字段as上”。groupBy指定按照哪个字段分组进行统计。<a href="#/cases/aggregate" target="_blank">查看示例</a>  
> 目前支持的聚合操作有： sum(求和)、mean(平均值)、min(最小值)、max(最大值)


**排序**

<a href="#/cases/sort" target="_blank">查看示例</a>
```
  {
    "type": "sort",
    "fields": ["year"],
    "orders": ["desc"]
  }
```

**计算字段**
```
  {
    "type": "map",
    "field": 'count',        // 字段
    "order": 'add',    // 计算操作
    "params": {
      "value": 100
    }            // 参数
  }
```

该操作将对数据中的某一个字段按照指定的指令和参数进行重算，返回新的字段值。

目前支持的操作和对应的参数： 
- toNumber(转为数值)

  无参数

- toString(转为字符串)

  无参数

- toUppercase(转为大写字母)

  无参数，字段类型必须为字符串

- toLowercase(转为小写字母)

  无参数，字段类型必须为字符串

- add(加上某一个值)

  参数`value`

- subtract(减去某一个值)

  参数`value`

- multiply(乘以某一个值)

  参数`value`

- divide(除以某一个值)

  参数`value`