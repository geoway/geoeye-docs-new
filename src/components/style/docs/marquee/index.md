## 类型
```
  "type": "marquee"
```
## paint属性

属性 | 说明 | 类型 | 默认值
--- | --- | --- | ---
text | 显示的文字内容，如果未指定`textField`，则直接显示该属性值 | String | -
textField | 显示的字段，必须设置了数据源才能使用该属性，用于显示数据源中的某一个字段值。设置了该属性会忽略`text`属性 | String | -
fontSize | 文字大小，单位`px` | Number | 18
fontColor | 文字颜色 | String | #ffffff
fontFamily | 字体 | String | Microsoft Yahei
bold | 是否加粗 | Boolean | false
italic | 是否显示为斜体 | Boolean | false
speed | 滚动的速度，从`1`到`10`，`1`最慢 | Number | 6


## 联动事件  
无
