## 类型
```
  "type": "bar"
```
## paint属性

```js
  paint: {
    style: 'normal',
    series: [{...}],
    legend: {...},
    grid: {...},
    tooltip: {...},
    xAxis: {...},
    yAxis: {...}
  }
```

### style

柱状图的样式。类型`Enum String`，可取值`normal`(普通柱状图)、`overlay`(堆叠柱状图)、`percent_overlay`（百分比堆叠柱状图），默认值`normal`。

### series[]

数据系列，类型`Object Array`


属性 | 说明 | 类型 | 默认值
--- | --- | --- | ---
name | 系列名称 | String | ''
barBorderRadius | 柱子的圆角半径，可以是数值表示四个角的半径，也可以是`[w,s,e,n]`格式的数组分别表示四个角的半径 | Number \| Array | 0
barWidth | 柱子的宽度 | Number | 15
barColor | 柱子的颜色，可以用多个颜色的数组表示渐变色 | String \| Array | ''
gradientDirection | 渐变颜色的方向，可取值`top` `bottom` `left` `right` `center` | Enum String | top
showLabel | 显示数值标注 | Boolean | false
labelColor | 标注的颜色 | String | 跟随barColor
labelSize | 标注的大小 | Number | 14
labelPosition | 标注的位置，可取值`top` `left` `right` `bottom` `inside` | Enum String | top
field | 数据字段 | String | ''
