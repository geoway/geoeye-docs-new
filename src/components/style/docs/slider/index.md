## 类型
```
  "type": "slider"
```

## paint属性

属性 | 说明 | 类型 | 默认值
--- | --- | --- | ---
pages | 图片的url列表 | Array | []
autoplay | 自动播放 | Boolean | true
interval | 自动播放间隔，单位`秒` | Number | 3


## 联动事件  
无
