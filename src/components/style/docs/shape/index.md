## 类型
```
  "type": "shape"
```
## paint属性

属性 | 说明 | 类型 | 默认值
--- | --- | --- | ---
style | 形状的样式，可取值`circle`(圆)、`rhombus`(菱形)、`hexagon`(六边形)、`hexagram`(六角星)、`pentagon`(五边形)、`octagon`(八边形)、`rectangle`(矩形)、`star`(五角星)、`triangle`(三角形)、`diamond`(钻石) | Enum String | star
borderColor | 边框颜色 | String | #ffffff
borderWidth | 边框宽度 | Number | 0
fillColor | 填充颜色，可以是颜色值或者颜色数组，颜色数组表示渐变色 | String | #0099ff
gradientDirection | 渐变颜色的方向，仅当fillColor为数组时有效，可取值`top` `bottom` `left` `right` `center` | Enum String | top

## 联动事件  
无
