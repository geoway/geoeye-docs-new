import Vue from 'vue'
import Layout from '@/components/share/Layout'
import MonacoEditor from '@/components/share/MonacoEditor'
import GeoeyeExample from '@/components/share/GeoeyeExample'
import RunCode from '@/components/share/RunCode'
import Card from '@/components/share/Card'
import MessageBox from '@/components/share/MessageBox'

Vue.component('geoeye-layout', Layout)
Vue.component('geoeye-monaco-editor', MonacoEditor)
Vue.component('geoeye-example', GeoeyeExample)
Vue.component('geoeye-run-code', RunCode)
Vue.component('geoeye-card', Card)
Vue.component('geoeye-message-box', MessageBox)