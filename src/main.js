import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './register.js'
import ViewUI from 'view-design'
import { VueGeoeye } from 'geoeye-js'
import '@/assets/css/style.css'
import 'view-design/dist/styles/iview.css'
import 'highlight.js/styles/default.css'
import 'geoeye-js/dist/geoeye.css'

Vue.config.productionTip = false
Vue.use(ViewUI)
Vue.component('Geoeye', VueGeoeye)
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

