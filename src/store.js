import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    activeMenu: '',
    error: {}
  },
  mutations: {
    update_menu(state, value) {
      state.activeMenu = value
    },
    show_error(state, error) {
      state.error = error
    }
  },
  actions: {

  }
})
