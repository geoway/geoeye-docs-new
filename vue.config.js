const marked = require('marked');
const hljs = require('highlight.js')
const MonacoWebpackPlugin = require('monaco-editor-webpack-plugin')

module.exports = {
  lintOnSave: false,
  publicPath: '',
  productionSourceMap: false,
  configureWebpack: config => {
    config.plugins.push(
      new MonacoWebpackPlugin({
        output: './js',
        languages: ['json', 'javascript'],
        features: ['find', 'folding', 'coreCommands']
      })
    )
    config.module.rules.push({
      test: /\.md$/,
      use: [
        {
          loader: "html-loader"
        },
        {
          loader: "markdown-loader",
          options: {
            renderer: new marked.Renderer(),
            gfm: true,
            tables: true,
            breaks: false,
            pedantic: false,
            sanitize: false,
            smartLists: true,
            smartypants: false,
            highlight: function (code, lang) {
              if (lang && hljs.getLanguage(lang)) {    
                return hljs.highlight(lang, code, true).value;
              } else {
                return hljs.highlightAuto(code).value;
              }
            }
          }
        }
      ]
    })
  },
  css: {
    loaderOptions: {
      less: {
        javascriptEnabled: true
      }
    }
  }
}
